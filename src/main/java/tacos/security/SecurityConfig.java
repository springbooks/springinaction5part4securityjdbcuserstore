package tacos.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

//    /**
//     * if we left configuration like this
//     * Spring security try to use this queries to get user information
//     *
//     * public static final String
//     * "select username,password,e
//     * "from users " +
//     * "where username = ?";
//     * public static final String
//     * "select username,authority
//     * "from authorities " +
//     * "where username = ?";
//     * public static final String
//     * "select g.id, g.group_name,
//     * "from groups g, group_membe
//     * "where gm.username = ? " +
//     * "and g.id = ga.group_id " +
//     * "and g.id = gm.group_id";
//     */
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication()
//                .dataSource(dataSource);
//    }

    /**
     * When replacing the default SQL queries with those of your own design, it’s important to adhere to the basic contract of the queries. All of them take the username as
     * their only parameter. The authentication query selects the username, password, and
     * enabled status. The authorities query selects zero or more rows containing the username and a granted authority. The group authorities query selects zero or more rows,
     * each with a group ID, a group name, and an authority.
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, enabled from Users where username = ?")
                .authoritiesByUsernameQuery("select username, authority from UserAuthorities where username = ?")
//                .groupAuthoritiesByUsername("...")
                .passwordEncoder(new StandardPasswordEncoder("53cr3t"));
    }

}
